export default (
    state = {
      itemgiphyDetail: {},
      isShowDialog: false
    },
    action
  ) => {
    switch (action.type) {
      case 'click_item':
        console.log('reducer', action)
        return {
          isShowDialog: true,
          itemgiphyDetail: action.payload
        };
      case 'dismiss_dialog':
        return {
          isShowDialog: false,
          itemgiphyDetail: {}
        };
      default:
        return state;
    }
  };
