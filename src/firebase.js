import firebase from 'firebase/app'
import 'firebase/app'
import 'firebase/database'
import 'firebase/auth'

const config = {
    apiKey: "AIzaSyCxE_QOUr8DSz44w98BQMK-ATHzDbwnGVM",
    authDomain: "react-68a5a.firebaseapp.com",
    databaseURL: "https://react-68a5a.firebaseio.com",
    projectId: "react-68a5a",
    storageBucket: "react-68a5a.appspot.com",
    messagingSenderId: "794102110498"
}

firebase.initializeApp(config)

const database = firebase.database()
const auth = firebase.auth()
const provider = new firebase.auth.FacebookAuthProvider()

export {
    database,
    auth,
    provider
}