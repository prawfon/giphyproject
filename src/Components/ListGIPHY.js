import React from 'react'
import ItemGifphy from './ItemGIPHY'
import { List } from 'antd'

function ListGiphy(props) {
    return (
        <div style={{ minHight: '300 px' }}>
            <List
                pagination={{
                    pageSize: 40,
                }}
                grid={{ gutter: 16, column: 4 }}
                dataSource={props.items}
                renderItem={item => (
                    <List.Item>
                        <ItemGifphy item={item} />
                    </List.Item>
                )}

            />
        </div>
    )
}
export default ListGiphy