import React,{ Component } from 'react';
import { Modal, Button, Avatar } from 'antd';
// import { auth } from '../../firebase';
// const KEY_USER_DATA = 'user-data';
// function getEmail() {
//     const jsonStr = localStorage.getItem(KEY_USER_DATA);
//     return JSON.parse(jsonStr).email;
// }

class Profile extends Component {
    state = {
        email: '',
        isShowDialog: false,
        imageUrl: ''
    }
    componentDidMount() {
        const jsonStr = localStorage.getItem('user-data')
        const email = jsonStr && JSON.parse(jsonStr).email;
        var imageUrl = jsonStr && JSON.parse(jsonStr).imageUrl;
        // console.log("ssss "+imageUrl)
        if (!imageUrl) {
          imageUrl = 'https://icons-for-free.com/free-icons/png/512/1902268.png';
        }
        this.setState({ email, imageUrl });
        // this.setState({ email: JSON.parse(jsonStr).email })
    }
    showDialogConfirmLogout = () => {
        this.setState({ isShowDialog: true })
    }
    handleCancel = () => {
        this.setState({ isShowDialog: false })
    }


    handleOk = () => {
        localStorage.setItem('user-data', JSON.stringify(
            {
                isShowDialog: false
            }
        ))
        this.props.history.push('/')
    }


    render() {
        // console.log("ssss "+this.state.imageUrl)
        return (
            <div>
                <Avatar size={128} src={this.state.imageUrl} />
                <h1>Email : {this.state.email}</h1>
                <Button  type="primary"
                 loading={this.state.isShowDialog}
                    onClick={this.showDialogConfirmLogout}>Logout</Button>
                <Modal
                    title="Confirm"
                    visible={this.state.isShowDialog}
                    onOk={this.handleOk}
                    onCancel={this.handleCancel}
                >
                    <p>Are you sure to logout?</p>
                </Modal>
            </div>
        );
    }
}
export default Profile;